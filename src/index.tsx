import * as React from 'react';
import * as ReactDom from 'react-dom';
import { AppComponent } from './components/app/app.component';
import { ConnectedSupplierSelectPage } from './pages/supplier-select/connected-supplier-select.page';
import { ConnectedSupplierDetailsPage } from './pages/supplier-details/connected-supplier-details.page';
import { createStore, applyMiddleware } from 'redux';
import { Provider} from 'react-redux';
import { reducer } from './reducer/index';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { CoJemyState } from './model/domain';
import { supplierService } from './service/supplier.service';
import { ReduxDevTools } from '../typings/redux-dev-tools';

import 'react-toolbox/lib/commons.scss';

const reduxDevTools: ReduxDevTools = require('redux-devtools-extension');

const initialState: CoJemyState = {
	suppliers: [],
	activeSupplierId: null
};

const store: any = createStore(
	reducer,
	initialState,
	reduxDevTools.composeWithDevTools(
		applyMiddleware(supplierService)
	)
);

ReactDom.render(
	<Provider store={store}>
		<Router history = {browserHistory}>
			<Route path='/' component={AppComponent}>
				<IndexRoute component={ConnectedSupplierSelectPage}/>
				<Route path='/supplier/:id' component={ConnectedSupplierDetailsPage} />
			</Route>
		</Router>
	</Provider>,
	document.getElementById('app')
);
