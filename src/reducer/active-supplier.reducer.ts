import { Reducer } from 'redux';
import * as React from 'react';
import { browserHistory } from 'react-router';

import {
	SELECT_SUPPLIER, SELECT_AND_GO_TO_SUPPLIER
} from '../model/actions';

import update = require('immutability-helper');

export const activeSupplierReducer: Reducer<any> = (state: any = [], action: any): any => {
	switch (action.type) {
		case SELECT_SUPPLIER:
			return action.supplierId;
		case SELECT_AND_GO_TO_SUPPLIER:
			browserHistory.push('/supplier/' + action.supplierId);
			return action.supplierId;
		default:
			return state;
	}
};
