import { Reducer, combineReducers } from 'redux';
import { activeSupplierReducer } from './active-supplier.reducer';
import { suppliersReducer } from './suppliers.reducer';

export const reducer: Reducer<any> = combineReducers({
	activeSupplierId: activeSupplierReducer,
	suppliers: suppliersReducer
});
