import { Reducer } from 'redux';
import * as React from 'react';

import {
	FETCH_SUPPLIERS_SUCCESS
} from '../model/actions';

import update = require('immutability-helper');

export const suppliersReducer: Reducer<any> = (state: any = [], action: any): any => {
	switch (action.type) {
		case FETCH_SUPPLIERS_SUCCESS:
			return action.suppliers;
		default:
			return state;
	}
};
