import {
	createFetchSuppliersAction, createSelectSupplierAction, SELECT_SUPPLIER, FETCH_SUPPLIERS,
	createSelectAndGoToSupplierAction, SELECT_AND_GO_TO_SUPPLIER
} from './actions';
import { expect } from 'chai';

describe("Actions ", () => {
	it('should create fetch suppliers action', () => {
		const expectedAction = {
			type: FETCH_SUPPLIERS
		};

		expect(createFetchSuppliersAction()).to.deep.equal(expectedAction);
	});

	it('should create select supplier action', () => {
		const expectedAction = {
			type: SELECT_SUPPLIER,
			supplierId: 1
		};

		expect(createSelectSupplierAction(1)).to.deep.equal(expectedAction);
	});

	it('should create select supplier action', () => {
		const expectedAction = {
			type: SELECT_AND_GO_TO_SUPPLIER,
			supplierId: 1
		};

		expect(createSelectAndGoToSupplierAction(1)).to.deep.equal(expectedAction);
	});
});
