import { Supplier } from './domain';

export const SELECT_SUPPLIER = 'SELECT_SUPPLIER';
export const SELECT_AND_GO_TO_SUPPLIER = 'SELECT_AND_GO_TO_SUPPLIER';
export const FETCH_SUPPLIERS = 'FETCH_SUPPLIERS';
export const FETCH_SUPPLIERS_SUCCESS = 'FETCH_SUPPLIERS_SUCCESS';
export const FETCH_SUPPLIERS_ERROR = 'FETCH_SUPPLIERS_ERROR';

export const createSelectSupplierAction: Function = (supplierId: number) => {
	return {
		type: SELECT_SUPPLIER,
		supplierId: supplierId
	};
};

export const createSelectAndGoToSupplierAction: Function = (supplierId: number) => {
	return {
		type: SELECT_AND_GO_TO_SUPPLIER,
		supplierId: supplierId
	};
};

export const createFetchSuppliersAction: Function = () => {
	return {
		type: FETCH_SUPPLIERS
	};
};

export const createFetchSuppliersSuccessAction: Function = (suppliers: Supplier[]) => {
	return {
		type: FETCH_SUPPLIERS_SUCCESS,
		suppliers: suppliers
	};
};

export const createFetchSuppliersErrorAction: Function = (error: any) => {
	return {
		type: FETCH_SUPPLIERS_ERROR,
		error: error
	};
};
