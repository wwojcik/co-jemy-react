export type Meal = {
	id: number,
	name: string
}

export type Supplier = {
	id: number,
	name: string,
	menu_items: Meal[]
}

export type CoJemyState = {
	suppliers: Supplier[],
	activeSupplierId: number
}
