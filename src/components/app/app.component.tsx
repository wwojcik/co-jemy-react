import * as React from 'react';

export class AppComponent extends React.Component<void, void> {
	public render(): JSX.Element {
		return (
			<div className='app-container'>
				{ this.props.children }
			</div>
		);
	}
}
