import * as React from 'react';
import { Supplier, Meal } from '../../model/domain';
import { Button } from 'react-toolbox/lib/button';
import { Card, CardTitle, CardActions } from 'react-toolbox/lib/card';
import { List, ListItem, ListSubHeader } from 'react-toolbox/lib/list';

type SupplierProps = {
	supplier: Supplier
}

export class SupplierComponent extends React.Component<SupplierProps, void> {
	public render(): JSX.Element {
		return (
			<Card>
				<CardTitle title={this.props.supplier.name} />
				<List>
					<ListSubHeader caption='Example meals'/>
					{
						this.props.supplier.menu_items.slice(0, 5).map((meal: Meal) => {
							return <ListItem key={meal.id} caption={meal.name}/>;
						})
					}
				</List>
				<CardActions>
					<Button label='Create order'/>
				</CardActions>
			</Card>
		);
	}
}
