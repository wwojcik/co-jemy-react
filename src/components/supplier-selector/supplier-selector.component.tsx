import * as React from 'react';
import { Dropdown } from 'react-toolbox/lib/dropdown';
import { Supplier } from '../../model/domain';

type SupplierSelectorProps = {
	suppliers: Supplier[],
	onSupplierSelect: Function
};

export class SupplierSelectorComponent extends React.Component<SupplierSelectorProps, void> {
	public render(): JSX.Element {
		const supplierItems: any[] = this.props.suppliers.map((supplier: Supplier) => {
			return {
				value: supplier.id,
				label: supplier.name
			};
		});

		return <Dropdown auto onChange={this.onChange.bind(this)} source={supplierItems} label='Select supplier'/>;
	}

	private onChange(value: any) {
		this.props.onSupplierSelect(+(value));
	}
}
