import { Middleware } from 'redux';
import { FETCH_SUPPLIERS, createFetchSuppliersErrorAction, createFetchSuppliersSuccessAction } from '../model/actions';

export const supplierService: Middleware = ({getState}: any) => {
	return (next: any) => (action: any) => {
		next(action);

		switch (action.type) {
			case FETCH_SUPPLIERS:
				console.log('Get suppliers');

				fetch ('http://co-jemy.herokuapp.com/api/suppliers', {mode: 'cors', method: 'GET'})
					.then(handleErrors)
					.then((response: any) => response.json())
					.then((data: any) => next(createFetchSuppliersSuccessAction(data)))
					.catch((error: any) => next(createFetchSuppliersErrorAction(error)));
				break;
			default:
				break;
		}
	};
};

const handleErrors: any = (response: IResponse) => {
	if (!response.ok) {
		throw Error(response.statusText);
	}
	return response;
};
