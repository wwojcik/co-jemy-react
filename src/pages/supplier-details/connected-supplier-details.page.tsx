import { connect } from 'react-redux';
import { SupplierDetailsPage } from './supplier-details.page';
import { CoJemyState, Supplier } from '../../model/domain';
import { createFetchSuppliersAction, createSelectSupplierAction } from '../../model/actions';

const mapStateToProps: any = (state: CoJemyState): Object => {
	return {
		supplier: state.suppliers.find((supplier: Supplier) => {
			return supplier.id === state.activeSupplierId;
		})
	};
};

const mapDispatchToProps: any = (dispatch: any): Object => {
	return {
		fetchSuppliers: () => {
			dispatch(createFetchSuppliersAction());
		},
		activateSupplier: (supplierId: number) => {
			dispatch(createSelectSupplierAction(supplierId));
		}
	};
};

export const ConnectedSupplierDetailsPage: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(SupplierDetailsPage);
