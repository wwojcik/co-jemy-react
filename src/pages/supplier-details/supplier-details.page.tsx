import * as React from 'react';
import { SupplierComponent } from '../../components/supplier/supplier.component';
import { Supplier } from '../../model/domain';
import { ProgressBar } from 'react-toolbox/lib/progress_bar';

type SupplierReviewPageProps = {
	params: {
		id: number
	},
	supplier: Supplier,
	fetchSuppliers: Function,
	activateSupplier: Function
}

export class SupplierDetailsPage extends React.Component<SupplierReviewPageProps, void> {
	public componentWillMount() {
		if (!this.props.supplier) {
			this.props.activateSupplier(+(this.props.params.id));
			this.props.fetchSuppliers();
		}
	}

	public render(): JSX.Element {
		if (this.props.supplier) {
			return <SupplierComponent supplier={this.props.supplier}/>;
		}

		return <ProgressBar type='linear' mode='indeterminate'/>;
	}
}
