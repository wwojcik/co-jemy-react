import { connect } from 'react-redux';
import { SupplierSelectPage } from './supplier-select.page';
import {
	createFetchSuppliersAction,
	createSelectAndGoToSupplierAction
} from '../../model/actions';
import { CoJemyState } from '../../model/domain';

const mapStateToProps: any = (state: CoJemyState): Object => {
	return {
		suppliers: state.suppliers
	};
};

const mapDispatchToProps: any = (dispatch: any): Object => {
	return {
		fetchSuppliers: () => {
			dispatch(createFetchSuppliersAction());
		},
		onSupplierSelect: (supplierId: number) => {
			dispatch(createSelectAndGoToSupplierAction(supplierId));
		}
	};
};

export const ConnectedSupplierSelectPage: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(SupplierSelectPage);
