import * as React from 'react';
import { SupplierSelectorComponent } from '../../components/supplier-selector/supplier-selector.component';
import { Supplier } from '../../model/domain';
import { ProgressBar } from 'react-toolbox/lib/progress_bar';

type SupplierSelectPageProps = {
	suppliers: Supplier[],
	onSupplierSelect: Function,
	fetchSuppliers: Function
}

export class SupplierSelectPage extends React.Component<SupplierSelectPageProps, void> {
	public componentWillMount() {
		if (!this.hasSuppliers()) {
			this.props.fetchSuppliers();
		}
	}

	public render(): JSX.Element {
		if (this.hasSuppliers()) {
			return (
				<div className='supplier-select-page'>
					<SupplierSelectorComponent suppliers={this.props.suppliers} onSupplierSelect={this.props.onSupplierSelect}/>
				</div>
			);
		}

		return <div className='loader'><ProgressBar type='linear' mode='indeterminate'/></div>;
	}

	private hasSuppliers() {
		return this.props.suppliers.length > 0;
	}
}
