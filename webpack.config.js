var webpack = require('webpack');

var plugins = [];

if (process.env.NODE_ENV === 'prod') {
	plugins.push(new webpack.optimize.UglifyJsPlugin({minimize: true}));
}

module.exports = {
	entry: ['es6-shim', './src/index.tsx'],
	output: {
		filename: "./dist/bundle.js"
	},

	devtool: "source-map",

	resolve: {
		extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
	},

	module: {
		loaders: [
			{ test: /\.json$/, loader: "json"},
			{ test: /\.tsx?$/, loader: "ts-loader" },
			{
				test: /\.s?css$/,
				loaders: ['style', 'css', 'sass'],
				exclude: /(node_modules)\/react-toolbox/
			},
			{
				test    : /(\.scss|\.css)$/,
				include : /(node_modules)\/react-toolbox/,
				loaders : [
					require.resolve('style-loader'),
					require.resolve('css-loader') + '?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
					require.resolve('sass-loader') + '?sourceMap'
				]
			}
		],
		preLoaders: [
			{ test: /\.js$/, loader: "source-map-loader" },
			{ test: /\.tsx?$/, loader: "tslint" }
		]
	},
	externals: {
		"react": "React",
		"react-dom": "ReactDOM"
	},
	plugins: plugins
};
