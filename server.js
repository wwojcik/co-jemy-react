const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');

const config = require('./webpack.dev.config');
const compiler = webpack(config);

const path = require('path');
const port = process.env.PORT || 8080;
const app = express();

app.use(
	'/static',
	express.static(__dirname + '/node_modules/react/dist'),
	express.static(__dirname + '/node_modules/react-dom/dist')
);

if (process.env.NODE_ENV === 'prod') {
	app.use('/app', express.static(__dirname + '/dist'));
} else {
	app.use(webpackDevMiddleware(compiler, {
		publicPath: config.output.publicPath,
		style: {colors: true}
	}));
}

app.use(webpackHotMiddleware(compiler, {
	log: console.log
}));

// handle every other route with index.html, which will contain
// a script tag to your application's JavaScript file(s).
app.get('*', function (request, response){
	response.sendFile(path.resolve(__dirname, 'app', 'index.html'))
});

app.listen(port);
console.log("server started on port " + port);
