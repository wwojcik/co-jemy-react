import { StoreEnhancer } from 'redux';

declare interface ReduxDevTools {
	composeWithDevTools(...args: any[]): StoreEnhancer<any>
}
