FROM node:latest
RUN apt-key adv --keyserver pgp.mit.edu --recv D101F7899D41F3C3 \
  && echo "deb http://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
  && apt-get update && apt-get install yarn \
  && apt-get -y autoremove \
  && apt-get -y clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*exit \
  && mkdir /app \
  && yarn global add pm2
WORKDIR /app
